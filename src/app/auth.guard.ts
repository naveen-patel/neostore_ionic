import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { DataService } from './data.service';
import { ToastController } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  constructor(
    private authService: DataService,
    private router: Router,
    public toastController: ToastController
  ) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    console.log('route', state.url)
    // window.sessionStorage.setItem('route', JSON.stringify(state.url));
    if (state.url.includes('/login')) {
      if (this.authService.loggedIn()) {
        this.router.navigate(['/home']);
        return false;
      } else {
        return true;
      }
    } else if ((state.url.includes('/account')) || (state.url.includes('/cart')) || (state.url.includes('/order'))|| (state.url.includes('/address'))) {
      if (this.authService.loggedIn()) {
        return true;
      } else {
        this.presentToast()
        window.sessionStorage.setItem('route', state.url);
        this.router.navigate(['/login']);
        return false;
      }
    } else {
      // console.log(state.url);
    }
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Please Login First!!!.',
      position: 'middle',
      duration: 2000
    });
    toast.present();
  }
}
