import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RatingModule } from 'ngx-rating';
import { ModalController } from '@ionic/angular';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports: [
    RatingModule,
    ModalController
  ]
})
export class SharedModule { }
