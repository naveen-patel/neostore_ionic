import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../environments/environment'
import { AlertController, Platform } from '@ionic/angular';
import { DataService } from '../data.service';
import swal from 'sweetalert'
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {
  data: any
  subImage: any;
  mainImage: any;
  rate = 2.5;
  url = environment.API_ENDPOINT;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public alertController: AlertController,
    private ds: DataService
  ) {
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.data = JSON.parse(params.special);
        this.mainImage = this.url + this.data.product_image
        this.subImage = this.data.subImages_id ? this.data.subImages_id.product_subImages : this.data.subImages[0].product_subImages;
      }
    });
  }
  ngOnInit() {
    let data = { address_id: 222, isDeliveryAddress: true }
    // add update demo 
    // this.ds.updateAdd(data).subscribe(data => {
    // })
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Sorry',
      subHeader: `<rating [(ngModel)]="rate" [max]="5"></rating>`,
      message: 'We are working on it !!',
      buttons: ['OK']
    });

    await alert.present();
  }
  saveCart() {
    console.log('data ', this.data);

    let data = [{ "_id": this.data._id, "product_id": this.data._id, "quantity": "1" }, { "flag": "logout" }]
    this.ds.orderProduct(data).subscribe(data => {
      swal("Added to cart!!!!! Thank you");
    })

    ////////for local storage//////////
    // if (!window.sessionStorage.getItem('cartData')) {
    //   let data = [];
    //   data.push(this.data);
    //   window.sessionStorage.setItem('cartData', JSON.stringify(data));
    // }
    // else {
    //   let id = this.data._id
    //   let cartData = JSON.parse(window.sessionStorage.getItem('cartData'));
    //   let check = cartData.filter(countryItem => countryItem._id === id);
    //   if (check.length !== 0) {
    //     alert("already in cart")
    //   } else {
    //     let data = [];
    //     let demo = [];
    //     demo = JSON.parse(window.sessionStorage.getItem('cartData'))
    //     window.sessionStorage.getItem('cartData') ? data = demo : []
    //     data.push(this.data);
    //     window.sessionStorage.setItem('cartData', JSON.stringify(data));
    //   }
    // }
  }

  changeImage(i) {
    this.mainImage = this.url + this.subImage[i];
  }
  onRateChange() { }
}
