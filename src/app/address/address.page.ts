import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import swal from 'sweetalert'
@Component({
  selector: 'app-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class AddressPage implements OnInit {
  data;
  constructor(private ds: DataService,
    // private sweetalert: swal
  ) { }

  ngOnInit() {
    this.ds.getAllAddress().subscribe(data => {
      this.data = data.customer_address;
    })
  }
  addAddress() {
    swal("Hello world!");
    console.log('add add')
  }
}
