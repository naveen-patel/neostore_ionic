import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  { path: 'all-product/:id', loadChildren: './all-product/all-product.module#AllProductPageModule' },
  { path: 'product-details', loadChildren: './product-details/product-details.module#ProductDetailsPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule', canActivate: [AuthGuard] },
  { path: 'account', loadChildren: './account/account.module#AccountPageModule', canActivate: [AuthGuard] },
  { path: 'cart', loadChildren: './cart/cart.module#CartPageModule', canActivate: [AuthGuard] },
  { path: 'order', loadChildren: './order/order.module#OrderPageModule', canActivate: [AuthGuard] },
  { path: 'registration', loadChildren: './registration/registration.module#RegistrationPageModule' },
  // { path: 'modal', loadChildren: './modal/modal.module#ModalPageModule' },
  { path: 'address', loadChildren: './address/address.module#AddressPageModule', canActivate: [AuthGuard]  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
