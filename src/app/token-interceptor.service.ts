import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { DataService } from './data.service';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
// import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {
  dataService;
  constructor(private injector: Injector, private route: Router) { }
  intercept(req: any, next: any): any {
    console.log('interceptor');
    this.dataService = this.injector.get(DataService);
    const tokenizedReq = req.clone({
      setHeaders: {
        Authorization: `${this.dataService.getToken()}`
      }
    });
    return next.handle(tokenizedReq).pipe(
      catchError((error: HttpErrorResponse) => {
        const errorMessage = '';
        if (error.error instanceof ErrorEvent) {
          // client-side error
          console.log('frontend-error');
          // this.toastr.error('OOPS!!..Something went wrong. Internal server error ');
          alert('Internal server error');
          // sessionStorage.clear();
          // this.route.navigate(['login']);
        } else {
          // this.toastr.error('OOPS!!..Something went wrong. We are working on getting this fixed as soon as we can. You may be able to try again.');
          console.log('Backend-error', error.error.message);
          alert(error.error.message);
          // sessionStorage.clear();
          // this.route.navigate(['login']);
        }
        return throwError(errorMessage);
      })
    );
  }
}