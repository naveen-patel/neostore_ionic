import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllProductPage } from './all-product.page';

describe('AllProductPage', () => {
  let component: AllProductPage;
  let fixture: ComponentFixture<AllProductPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllProductPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllProductPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
