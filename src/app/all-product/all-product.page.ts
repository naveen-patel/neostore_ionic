import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service'
import { environment } from '../../environments/environment'
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-all-product',
  templateUrl: './all-product.page.html',
  styleUrls: ['./all-product.page.scss'],
})
export class AllProductPage implements OnInit {
  url = environment.API_ENDPOINT
  url1 = '/product-details'
  product;
  data;
  title = "Products";
  constructor(
    private ds: DataService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.data = JSON.parse(params.special);
        // this.title = this.data.title
      }
    });
  }
  ngOnInit() {
    if (this.data) {
      this.title = this.data[0].category_name
      this.allProduct(this.data[0]._id)
    }
    else {
      this.title = 'Products';
      this.allProduct('')
    }
  }

  allProduct(id) {
    this.ds.getAllProduct(id).subscribe(data => {
      if (data.status_code == 200) {
        this.product = data.product_details;
      } else {
      }

    });
  }
  openDetailsWithQueryParams(i) {
    let data = this.product[i];
    console.log(data)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(data)
      }
    };
    this.router.navigate(['product-details'], navigationExtras);
  }
}
