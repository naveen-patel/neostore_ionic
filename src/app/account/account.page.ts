import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

import { AddressModuleModule } from '../../app/address-module/address-module.module'
@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  profileImage;
  data;
  constructor(
    private router: Router,
    public alertController: AlertController,

  ) { }

  ngOnInit() {
    let data = window.sessionStorage.getItem('userData');
    this.data = JSON.parse(data);
    if (1) {
      this.profileImage = true
    } else {

    }
  }
  logout() {
    window.sessionStorage.clear();
    this.router.navigateByUrl('/home');
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Sorry',
      subHeader: ``,
      message: 'We are working on it !!',
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentModal() {
    // const modal = await this.modalController.create({
    //   component: AddressModuleModule
    // });
    // return await modal.present();
  }
}
