// import { Injectable } from '@angular/core';
// import { Observable } from 'rxjs';
// import 'rxjs/Rx';
import { environment } from '../environments/environment'
// import {
//   HttpClient
// } from "@angular/common/http";

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, ErrorObserver } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {


  url = environment.API_ENDPOINT;
  categories;
  constructor(private http: HttpClient, ) { }

  // for dashboard data commonProducts?category_id=5cfe3c6fea821930af692820

  getAllProduct(id): Observable<any> {
    return this.http.get(this.url + 'commonProducts?category_id=' + id);
  }

  // for all category details
  getAllCategories(): Observable<any> {
    return this.http.get(this.url + 'getAllCategories');
  }

  // set data for product
  setData(data) {

  }
  // get product data
  getData() {
    return this.categories;
  }
  getToken() {
    const tokens = JSON.parse(window.sessionStorage.getItem('token'));
    if (tokens != null) {
      return 'bearer ' + tokens;
    } else {
      return false;
    }
  }

  loggedIn() {
    return !!sessionStorage.getItem('token');
  }
  //For user registation
  register(value): Observable<any> {
    return this.http.post(this.url + 'register', value);
  }
  // for user login
  login(value): Observable<any> {
    return this.http.post(this.url + 'login', value);
  }
  // for all address
  getAllAddress(): Observable<any> {
    return this.http.get(this.url + 'getCustAddress');
  }
  // for add data in cart && order
  orderProduct(value): Observable<any> {
    return this.http.post(this.url + 'addProductToCartCheckout', value);
  }
  // for select address for delivery
  updateAdd(value): Observable<any> {
    return this.http.put(this.url + 'updateAddress', value);
  }
  //For get cart data
  cartData(): Observable<any> {
    return this.http.get(this.url + 'getCartData');
  }
  //for delete product from cart
  // http://180.149.241.208:3022/deleteCustomerCart/5d0b1d694594d26e47774b5c
  cartDataDelete(value): Observable<any> {
    let id = value
    return this.http.delete(this.url + 'deleteCustomerCart/' + id);
  }
  // for dashboard slide 
  dashboardSlid(): Observable<any> {
    return this.http.get(this.url + 'getAllCategories');
  }
  // for top rated product 
  topRatingProduct(): Observable<any> {
    return this.http.get(this.url + 'defaultTopRatingProduct');
  }
}
