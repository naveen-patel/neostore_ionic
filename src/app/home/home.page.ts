import { Component } from '@angular/core';
import { DataService } from '../data.service'
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { NavigationExtras, Router } from '@angular/router';
import { environment } from '../../environments/environment'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  baseURL = environment.API_ENDPOINT;
  loop = [{ 'id': 1 }, { 'id': 2 }, { 'id': 3 }, { 'id': 5 }, { 'id': 6 }];
  data;
  product;
  cartCount = false;
  cart = [];
  items = [];
  topRated;
  sliderConfig = {
    slidesPerView: 1.6,
    spaceBetween: 10,
    centeredSlides: true,
    loop: true,
    autoplay: true,
    pager: true
  };

  constructor(
    private ds: DataService,
    private route: Router,
    private faio: FingerprintAIO
  ) { 
    this.faio.show({
      clientId: 'Fingerprint-Demo', //Android: Used for encryption. iOS: used for dialogue if no `localizedReason` is given.
      clientSecret: 'o7aoOMYUbyxaD23oFAnJ', //Necessary for Android encrpytion of keys. Use random secret key.
      disableBackup:true,  //Only for Android(optional)
      localizedFallbackTitle: 'Use Pin', //Only for iOS
      localizedReason: 'Please authenticate' //Only for iOS
  })
  .then((result: any) => console.log('result',result))
  .catch((error: any) => console.log('error',error));
  }
  ngOnInit() {
    this.allProduct()
    let a = this.ds.getData();
    let check = window.sessionStorage.getItem('token');
    if (check) {
      this.cartCount = true;
    }
    // for category name 
    this.ds.dashboardSlid().subscribe((res) => {
      this.data = res.category_details;
    })
    // for top rating product
    this.ds.topRatingProduct().subscribe((res) => {
      this.topRated = res.product_details;
      console.log(this.topRated);

    })
  }

  allProduct() {
    this.ds.getAllProduct('').subscribe(data => {
      if (data.status_code == 200) {
        this.product = data.product_details;
      } else {

      }
    });
  }
  forSelect(i) {
    let a = [];
    a.push(this.data[i])
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(a)
      }
    };
    this.route.navigate(['all-product/' + a[0].category_name], navigationExtras);
  }

  openDetailsWithQueryParams(i) {
    let data = this.topRated[i].DashboardProducts[0];
    console.log('22222222222', data)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(data)
      }
    };
    this.route.navigate(['product-details'], navigationExtras);
  }

  
}
