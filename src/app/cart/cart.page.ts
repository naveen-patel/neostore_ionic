import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DataService } from '../data.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
  data;
  quantity;
  url = environment.API_ENDPOINT;

  users: any[] = [
    {
      id: 1,
      value: 1
    },
    {
      id: 2,
      value: 2
    },
    {
      id: 3,
      value: 3
    }, {
      id: 4,
      value: 4
    }, {
      id: 5,
      value: 5
    }, {
      id: 6,
      value: 6
    }, {
      id: 7,
      value: 7
    }, {
      id: 8,
      value: 8
    }, {
      id: 9,
      value: 9
    }, {
      id: 10,
      value: 10
    }
  ];
  compareWith = this.quantity;
  constructor(
    private ds: DataService
  ) { }

  ngOnInit() {
    this.ds.cartData().subscribe(data => {
      this.data = data.product_details;
    })
  }
  deleteProduct(id) {
    this.ds.cartDataDelete(id).subscribe(data => {
      if (data.status_code == 200) {
        this.ngOnInit();
      }
    })
  }
  check(i) {
    this.quantity = this.data[i].quantity
  }
  selectqyt() {
  }
}
