import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DataService } from '../data.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  flag = false;
  model: any = {};
  constructor(
    private router: Router,
    private ds: DataService
  ) { }

  ngOnInit() {
  }

  login() {
    console.log(this.model);

    this.ds.login(this.model).subscribe((res) => {
      console.log('response', res);
      window.sessionStorage.setItem('token', JSON.stringify(res.token));
      delete res.token;
      window.sessionStorage.setItem('userData', JSON.stringify(res));
      let route = window.sessionStorage.getItem('route')
      this.router.navigateByUrl(route);
    });
  }


}
