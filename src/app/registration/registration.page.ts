import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { DataService } from '../data.service'
@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  constructor(
    private router: Router,
    private ds: DataService
  ) { }

  ngOnInit() {
  }
  register(form) {
    console.log('in regis', form);
    this.ds.register(form.value).subscribe((res) => {
      this.router.navigateByUrl('/login');
    });
  }
}
