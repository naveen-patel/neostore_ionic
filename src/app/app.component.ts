import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DataService } from '../app/data.service';
import { Router, NavigationExtras } from '@angular/router';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'All Product',
      url: '/all-product/0',
      icon: 'list'
    },
    {
      title: 'Chair',
      url: '/all-product/1',
      icon: 'list'
    },
    {
      title: 'Bed',
      url: '/all-product/2',
      icon: 'bed'
    },
    {
      title: 'Sofa',
      url: '/all-product/3',
      icon: 'list'
    },
    {
      title: 'Table',
      url: '/all-product/4',
      icon: 'list'
    },
    {
      title: 'Almirah',
      url: '/all-product/5',
      icon: 'list'
    },
    {
      title: 'My Account',
      url: '/account',
      icon: 'person'
    }, {
      title: 'My Cart',
      url: '/cart',
      icon: 'cart'
    }, {
      title: 'My Order',
      url: '/order',
      icon: 'list'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];

  encryptMode: boolean;
  textToConvert: string = 'naveen'
  password: string = '123456'
  conversionOutput: string = "naveen"
  option = true;
  categories;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private ds: DataService,
    private router: Router
  ) {
    this.initializeApp();
    this.encryptMode = true;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  ngOnInit() {
    this.categoriesData();
    this.convertText();
  }
  categoriesData() {
    this.ds.getAllCategories().subscribe(data => {
      if (data.status_code == 200) {
        this.categories = data.category_details;
        this.ds.setData(this.categories)
      } else {

      }
    });
  }
  forSelect(title) {
    if (title == "Sofa" || title == "Table" || title == "Chair" || title == "Almirah" || title == "Bed") {
      let a = this.categories.filter(countryItem => countryItem.category_name === title);
      console.log("aaaaaaaaaaaaaaaaaa", a);

      let navigationExtras: NavigationExtras = {
        queryParams: {
          special: JSON.stringify(a)
        }
      };
      this.router.navigate(['all-product/' + title], navigationExtras);
    }
  }
  willOpen() {
    let check = window.sessionStorage.getItem('token');
    if (check) {
      this.option = false;
    }
    else { this.option = true; }
  }
  click(i) {
    if (i) {
      console.log("login");
      this.router.navigate(['login']);
    } else {
      console.log("regis");
      this.router.navigate(['registration']);
    }
  }

  convertText() {
    if (this.encryptMode) {
      this.conversionOutput = CryptoJS.AES.encrypt(this.textToConvert.trim(), this.password.trim()).toString();
    }
    else {
      this.conversionOutput = CryptoJS.AES.decrypt(this.textToConvert.trim(), this.password.trim()).toString(CryptoJS.enc.Utf8);
    }

    console.log('first', this.conversionOutput);
    this.conversionOutput = CryptoJS.AES.decrypt(this.textToConvert.trim(), this.password.trim()).toString(CryptoJS.enc.Utf8);
    console.log('second', this.conversionOutput);
  }
}
